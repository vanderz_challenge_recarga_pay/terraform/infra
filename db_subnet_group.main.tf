resource "aws_db_subnet_group" "main" {
  description = "Subnetes privadas do Recarga Pay"
  name        = "recarga_pay_private_subnetes"
  subnet_ids  = [aws_subnet.private_1c.id, aws_subnet.private_1d.id]
}
