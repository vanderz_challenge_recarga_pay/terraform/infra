variable "table_name" {
  default = "recarga_pay_usuario"
}

variable "hash_key" {
  default = "UserId"
}

variable "read_capacity" {
  default = 20
}

variable "write_capacity" {
  default = 20
}

variable "attribute" {
  default = {
    "attributes" = {
      name = "UserId"
      type = "S"
    }
  }
}

variable "tags" {
  default = {}
}
