# terraform/infra

## Solution Architecture

The image below shows the solution architecture designed for this project:

![Alt text](images/solution-architecture.png?raw=true "Solution Architecture")

This solution is proposed to meet the following requirements in all infrastructure:

* high availability
  * ECS Cluster deployed in two AZ
  * RDS Multi-AZ. If the master database fails, an slave databe will assume. This is not implemented in this project to reduce cost, but in a real world, this needs to be implemented.
  * Two subnets for each private and public network
  * Elastic Load Balancer (NLB) to balancer the load to the nodes (containers)
* escability
  * ECS Cluster to scale in or out the compute capacity according to the CPU metric. Only CPU metric was a choiced to reduce time to implement this solution.
  * RDS to scale up disk storage and compute capacity
* reliability
  * ECS Cluster to scale in or out the compute capacity according to the CPU metric.
  * ECS Cluster deployed as a Fargate type to automatically recover from failure.
  * RDS Multi-AZ.
  * Terraform to automate the changes in infrastructure
  * ECS and RDS are monitored and alarmed by Cloudwatch and SNS. Only a few metrics was implanted to reduce time to implement this solution.
* resilience
  * ECS Cluster deployed as a Fargate type. Also you can easily change compute capacity when you want.
  * RDS Multi-AZ. Also you can easily change compute capacity when you want.
* security
  * WAF to protect web calls that API will receive from external requests.
  * ECS and RDS was deployed in privates subnets, avoind external access
  * API Gateway controls the requests. Only request that has an api-key on header can get the right response. Also, it has throttling and quote enabled for the specific api-key.

WAF, API Gateway, SNS, Cloudwatch, DynamoDB and ECR are services that AWS manage their infrastructure, so, these solutions meet all requirementes mecioned.

## CI/CD Pipeline

This project has terraform code for these AWS resources:

* VPC: deploys the network designed architecture
* RDS: deploys Mysql relational database
* DynamoDB: deploys DynamoDB nosql database
* ECR: deploys Container Registry and the repository where the container images will rely
* Cloudwatch: monitors and alarms RDS events
* SNS: sends e-mail notification when an alarm occur

A Gitlab backend is used to manage terraform state.

Five jobs are responsible to apply these terraform files to AWS:

* **init**: performs terraform init
* **validate**: performs terraform validate
* **build**: performs terraform plan
* **deploy**: performs terraform apply (manual execution)
* **cleanup**: performs terraform destroy (manual execution)

More details about the pipeline flow, see README from springboot repository:

https://gitlab.com/vanderz_challenge_recarga_pay/springboot

## Monitoring and alarms

The pictures below show some monitoring and alarms that were deployed at AWS by this pipeline:

![Alt text](images/CloudWatch-ECSAlarms.png?raw=true "ECS monitoring and alarms")

![Alt text](images/CloudWatch-RDSAlarms.png?raw=true "RDS monitoring and alarms")

Here some pictures related to the alarm notification process:

![Alt text](images/sns-subscription-notification.png?raw=true "E-mail subscription notification")

![Alt text](images/alarm-notification.png?raw=true "Alarm notification")

