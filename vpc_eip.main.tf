resource "aws_eip" "public_1c" {
  network_border_group = var.region
  public_ipv4_pool     = "amazon"

  tags = {
    Name = "recarga_pay"
  }

  vpc = "true"
}

resource "aws_eip" "public_1d" {
  network_border_group = var.region
  public_ipv4_pool     = "amazon"

  tags = {
    Name = "recarga_pay"
  }

  vpc = "true"
}
