variable "db_instance_name" {}

variable "db_instance_username" {}

variable "db_instance_password" {}

variable "db_instance_allocated_storage" {
  default = "20"
}

variable "db_instance_auto_minor_version_upgrade" {
  default = "true"
}

variable "db_instance_backup_retention_period" {
  default = "0"
}

variable "db_instance_backup_window" {
  default = "09:35-10:05"
}

variable "db_instance_ca_cert_identifier" {
  default = "rds-ca-2019"
}

variable "db_instance_copy_tags_to_snapshot" {
  default = "false"
}

variable "db_instance_deletion_protection" {
  default = "false"
}

variable "db_instance_engine" {
  default = "mysql"
}

variable "db_instance_engine_version" {
  default = "5.7.31"
}

variable "db_instance_iam_database_authentication_enabled" {
  default = "false"
}

variable "db_instance_instance_class" {
  default = "db.t2.micro"
}

variable "db_instance_iops" {
  default = "0"
}

variable "db_instance_license_model" {
  default = "general-public-license"
}

variable "db_instance_maintenance_window" {
  default = "tue:07:29-tue:07:59"
}

variable "db_instance_max_allocated_storage" {
  default = "1000"
}

variable "db_instance_monitoring_interval" {
  default = "0"
}

variable "db_instance_multi_az" {
  default = "false"
}

variable "db_instance_option_group_name" {
  default = "default:mysql-5-7"
}

variable "db_instance_parameter_group_name" {
  default = "default.mysql5.7"
}

variable "db_instance_performance_insights_enabled" {
  default = "false"
}

variable "db_instance_performance_insights_retention_period" {
  default = "0"
}

variable "db_instance_port" {
  default = "3306"
}

variable "db_instance_publicly_accessible" {
  default = "false"
}

variable "db_instance_storage_encrypted" {
  default = "false"
}

variable "db_instance_storage_type" {
  default = "gp2"
}

variable "db_instance_skip_final_snapshot" {
  default = true
}