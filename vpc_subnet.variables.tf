variable "subnet_public_1c_cidr_block" {
  default = "10.0.0.0/27"
}

variable "subnet_private_1c_cidr_block" {
  default = "10.0.0.32/27"
}

variable "subnet_public_1d_cidr_block" {
  default = "10.0.0.96/27"
}

variable "subnet_private_1d_cidr_block" {
  default = "10.0.0.128/27"
}