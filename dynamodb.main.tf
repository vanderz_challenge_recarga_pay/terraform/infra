resource "aws_dynamodb_table" "main" {
  name = var.table_name
  hash_key = var.hash_key
  read_capacity = var.read_capacity
  write_capacity = var.write_capacity

  dynamic "attribute" {
    for_each = var.attribute
    content {
      name = attribute.value.name
      type   = attribute.value.type
    }
  }

  tags = var.tags
}