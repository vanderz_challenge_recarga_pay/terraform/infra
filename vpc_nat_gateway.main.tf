resource "aws_nat_gateway" "public_1c" {
  allocation_id = aws_eip.public_1c.id
  subnet_id     = aws_subnet.public_1c.id
}

resource "aws_nat_gateway" "public_1d" {
  allocation_id = aws_eip.public_1d.id
  subnet_id     = aws_subnet.public_1d.id
}
