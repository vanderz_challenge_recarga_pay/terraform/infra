resource "aws_route_table" "public" {
    vpc_id = aws_vpc.main.id
    
    route {
        cidr_block = "0.0.0.0/0" 
        gateway_id = aws_internet_gateway.main.id
    }
    
    tags = {
        Name = "recarga_pay"
    }
}

resource "aws_route_table" "private_1c" {
    vpc_id = aws_vpc.main.id
    
    route {
        cidr_block = "0.0.0.0/0" 
        nat_gateway_id = aws_nat_gateway.public_1c.id
    }
    
    tags = {
        Name = "recarga_pay"
    }
}

resource "aws_route_table" "private_1d" {
    vpc_id = aws_vpc.main.id
    
    route {
        cidr_block = "0.0.0.0/0" 
        nat_gateway_id = aws_nat_gateway.public_1d.id
    }
    
    tags = {
        Name = "recarga_pay"
    }
}

resource "aws_route_table_association" "public-subnet-1c"{
    subnet_id = aws_subnet.public_1c.id
    route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "public-subnet-1d"{
    subnet_id = aws_subnet.public_1d.id
    route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "private-subnet-1c"{
    subnet_id = aws_subnet.private_1c.id
    route_table_id = aws_route_table.private_1c.id
}

resource "aws_route_table_association" "private-subnet-1d"{
    subnet_id = aws_subnet.private_1d.id
    route_table_id = aws_route_table.private_1d.id
}