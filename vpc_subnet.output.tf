output "subnet_public_1c_id" {
  value = aws_subnet.public_1c.id
}

output "subnet_public_1d_id" {
  value = aws_subnet.public_1d.id
}

output "subnet_private_1c_id" {
  value = aws_subnet.private_1c.id
}

output "subnet_private_1d_id" {
  value = aws_subnet.private_1d.id
}
