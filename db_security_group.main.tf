resource "aws_security_group" "db_instance" {
  description = "Acesso ao banco de dados"

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "0"
    protocol    = "-1"
    self        = "false"
    to_port     = "0"
  }

  ingress {
    cidr_blocks = [ var.subnet_private_1c_cidr_block, var.subnet_private_1d_cidr_block]
    description = "Acesso da rede privada"
    from_port   = var.db_instance_port
    protocol    = "tcp"
    self        = "false"
    to_port     = var.db_instance_port
  }

  name = "recarga_pay_db_access"

  tags = {
    Name = "recarga_pay"
  }

  vpc_id = aws_vpc.main.id
}
