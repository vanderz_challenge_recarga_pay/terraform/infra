output "dynamodb_id" {
  value = aws_dynamodb_table.main.id
}

output "dynamodb_arn" {
  value = aws_dynamodb_table.main.arn
}