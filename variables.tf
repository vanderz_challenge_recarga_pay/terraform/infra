variable "region" {
  default = "us-east-1"
}

variable "az_1c" {
  default = "us-east-1c"
}

variable "az_1d" {
  default = "us-east-1d"
}

variable "account" {
  default = "154159259755"
}