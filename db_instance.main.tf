resource "aws_db_instance" "main" {
  allocated_storage                     = var.db_instance_allocated_storage
  auto_minor_version_upgrade            = var.db_instance_auto_minor_version_upgrade
  availability_zone                     = var.az_1c
  backup_retention_period               = var.db_instance_backup_retention_period
  backup_window                         = var.db_instance_backup_window
  ca_cert_identifier                    = var.db_instance_ca_cert_identifier
  copy_tags_to_snapshot                 = var.db_instance_copy_tags_to_snapshot
  db_subnet_group_name                  = aws_db_subnet_group.main.name
  deletion_protection                   = var.db_instance_deletion_protection
  engine                                = var.db_instance_engine
  engine_version                        = var.db_instance_engine_version
  iam_database_authentication_enabled   = var.db_instance_iam_database_authentication_enabled
  identifier                            = var.db_instance_name
  instance_class                        = var.db_instance_instance_class
  iops                                  = var.db_instance_iops
  license_model                         = var.db_instance_license_model
  maintenance_window                    = var.db_instance_maintenance_window
  max_allocated_storage                 = var.db_instance_max_allocated_storage
  monitoring_interval                   = var.db_instance_monitoring_interval
  multi_az                              = var.db_instance_multi_az
  option_group_name                     = var.db_instance_option_group_name
  parameter_group_name                  = var.db_instance_parameter_group_name
  performance_insights_enabled          = var.db_instance_performance_insights_enabled
  performance_insights_retention_period = var.db_instance_performance_insights_retention_period
  port                                  = var.db_instance_port
  publicly_accessible                   = var.db_instance_publicly_accessible
  storage_encrypted                     = var.db_instance_storage_encrypted
  storage_type                          = var.db_instance_storage_type
  skip_final_snapshot                   = var.db_instance_skip_final_snapshot
  username                              = var.db_instance_username
  password                              = var.db_instance_password
  vpc_security_group_ids                = [aws_security_group.db_instance.id]
}

module "rds-alarms" {
  source  = "lorenzoaiello/rds-alarms/aws"
  version = "2.0.0"
  db_instance_id    = aws_db_instance.main.id
  db_instance_class = var.db_instance_instance_class
  actions_alarm = [ aws_sns_topic.alarm.arn ]
}