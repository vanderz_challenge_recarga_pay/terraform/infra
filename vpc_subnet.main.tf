resource "aws_subnet" "public_1c" {
  assign_ipv6_address_on_creation = "false"
  cidr_block                      = var.subnet_public_1c_cidr_block
  map_public_ip_on_launch         = "true"
  availability_zone               = var.az_1c

  tags = {
    Name = "Public subnet 1c"
  }

  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "private_1c" {
  assign_ipv6_address_on_creation = "false"
  cidr_block                      = var.subnet_private_1c_cidr_block
  map_public_ip_on_launch         = "false"
  availability_zone               = var.az_1c

  tags = {
    Name = "Private subnet 1c"
  }

  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "public_1d" {
  assign_ipv6_address_on_creation = "false"
  cidr_block                      = var.subnet_public_1d_cidr_block
  map_public_ip_on_launch         = "true"
  availability_zone               = var.az_1d

  tags = {
    Name = "Public subnet 1d"
  }

  vpc_id = aws_vpc.main.id
}

resource "aws_subnet" "private_1d" {
  assign_ipv6_address_on_creation = "false"
  cidr_block                      = var.subnet_private_1d_cidr_block
  map_public_ip_on_launch         = "false"
  availability_zone               = var.az_1d

  tags = {
    Name = "Private Subnet 1d"
  }

  vpc_id = aws_vpc.main.id
}
