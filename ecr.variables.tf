variable "repository_name" {}

variable "untagged_count_number" {
  default = 1
}

variable "tagged_count_number" {
  default = 3
}